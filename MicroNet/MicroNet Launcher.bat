title MicroNet Launcher
echo Testing Text Filters
@echo off
echo Before starting MicroNet you need to make sure you have the files listed below:
echo MicroLauncher.lzma
echo MicroNet.lzma
echo.
echo If you don't have ALL of these files please check at MicroStalk.do.am
echo.
echo When closing please type quit so the lzma files will be converted back from .zip to .lzma
echo.
echo You can close using stop but you might get an internal error saying that the lzmas were converted to .microstalk files
pause
goto :lzmaConvert
:lzmaConvert
echo Converting MicroLauncher.lzma
rename MicroLauncher.lzma ML.zip
echo Converting MicroNet.lzma
rename MicroNet.lzma MN.zip
:main
cls
echo Please make sure the plugins are loaded correctly
set /p MNSU=Do you have MicroNetSpeedUp.lzma (enter Yes or No)?
if %MNSU%==Yes goto :speedcheck
if %MNSU%==No goto :failure
:failure
echo You need MicroNetSpeedUp.lzma in order to launch all protocols!
pause
goto :internelstop
:internelstop
echo Error: 45x006
rename *.zip Protocol45x006.microstalk
echo all lzmas were converted to .microstalk
pause
:quit
echo Compressing all lzmas
rename MN.zip MicroNet.lzma
rename ML.zip MicroLauncher.lzma
exit
:speedcheck
goto :cmdprompt
:cmdprompt
set /p miron=type quit or stop
if %miron%==quit goto :quit
if %miron%==stop goto :internelstop